after reboot

#!/bin/bash
kill -HUP $(ps -A -ostat,ppid | grep -e '[zZ]'| awk '{ print $2 }')
docker start $(docker ps -a -q)
cd /opt/tomcat/latest/bin
sh startup.sh
runuser -l jenkins -c 'pm2 list'
sh free.sh
sh zombieKill.sh --admin 