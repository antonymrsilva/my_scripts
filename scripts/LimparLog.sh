#! /bin/sh

cd /var/log
#limpa arquivos compactados

compact=`find -name "*.gz" | wc -l`
if [ $compact -gt 0 ]
then
 
 #Procura tudo que for .gz log compactatdo dentro do /var/log
 compact2=`find -iname "*.gz"`
 
 #apaga arquivos compactatdos
 for apaga in $compact2
 do
 rm -f $apaga
 done
  
 # cria lista de todos os arquivos de log que serao limpos
 lista=`find -type f`

 # executa a limpeza dos logs
 for i in $lista
 do
 echo -n >$i &>/dev/null
 done

else

 # cria lista de todos os arquivos de log que serao limpos
 lista=`find -type f`
 # executa a limpeza dos logs
 for i in $lista
 do
 echo -n >$i &>/dev/null
 done
fi
