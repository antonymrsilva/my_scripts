#!/bin/bash
sudo docker ps -a

echo "Digite o nome do projeto igual ao digitado no Jenkins:"
read nome_projeto

echo "Digite a porta do servidor:"
read porta

docker run -d -v /home/antony/$nome_projeto:/var/www/html/ -p $porta:80 --name $nome_projeto php:7.2-apache
if [ $? -eq 0 ]; then
    echo "Container criado com o nome $nome_projeto mapeado na porta $porta."

    touch /home/antony/$nome_projeto/.htaccess

    echo "RewriteEngine on
    RewriteCond %{REQUEST_FILENAME} -s [OR]
    RewriteCond %{REQUEST_FILENAME} -l [OR]
    RewriteCond %{REQUEST_FILENAME} -d
    RewriteRule ^.*$ - [NC,L]

    RewriteRule ^(.*) /index.html [NC,L]" >> /home/antony/$nome_projeto/.htaccess
    chmod 777 /home/antony/$nome_projeto

    
    docker exec $nome_projeto a2enmod rewrite
    docker exec $nome_projeto service apache2 restart
            
        if [$? -eq 0]; then
            docker start $nome_projeto
            if [$? -eq 0]; then
                echo "***CONTAINER CRIADO COM SUCESSO***"
            else 
                echo "***OCORREU ALGUM ERRO, TENTE NOVAMENTE***"
            fi
        fi

else
    echo "**OCORREU ALGUM ERRO, TENTE NOVAMENTE**"

fi
