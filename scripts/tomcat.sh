#!/bin/bash

# INSTALAR O TOMCAT 9.0.40

mv tomcat-9.tar.xz /opt

cd /opt

tar -xvf tomcat-9.tar.xz

rm tomcat-9.tar.xz

cd tomcat-9/bin

chmod +x *.sh

sh startup.sh
