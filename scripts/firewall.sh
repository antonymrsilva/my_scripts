#!/bin/bash

#LINK ARTIGO UFW = https://www.digitalocean.com/community/tutorials/how-to-setup-a-firewall-with-ufw-on-an-ubuntu-and-debian-cloud-server
#LIBERANDO AS PORTAS PADRÕES USADAS NO PROJETO

ufw default deny incoming
ufw default allow outgoing
ufw status verbose
ufw allow 22
ufw allow 80
ufw allow 8080
ufw allow 443
ufw enable
